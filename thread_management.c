#include <stdio.h>
#include <pthread.h>
#include <errno.h>
#include <unistd.h>

void *thread_start(void *args)
{
    int val;
    val = pthread_detach(pthread_self());
    if (val)
    {
        printf("pthread_detach error\n");
    }
    pthread_exit(NULL);
}

int main(void)
{
    pthread_t threadID;
    int ret;
    long count = 0;
    void *retval;

    while(1)
    {
        ret = pthread_create(&threadID, NULL, thread_start, NULL);
        if (ret)
        {
            printf("pthread_create() fail ret : %d\n", ret);
            perror("Fail reason:");
            break;
        }
        usleep(10);
        //pthread_join(threadID, &retval);
        count++;
        if ((count % 10000) == 0)
        {
            printf("Number of thread are created : %ld\n", count);
        }
        
    }
    printf("Number of thread are created : %ld\n", count);
    return 0;
}